//
//  ViewController.swift
//  Actividad3.1
//
//  Created by Tecmilenio on 29/01/19.
//  Copyright © 2019 jfrn. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource  {

    var paises = ["Afganistan","Albania","Alemania","Andorra","Angola","Antartida","AntiguayBarbuda","ArabiaSaudi","Argelia","Argentina","Armenia","Australia","Austria","Azerbaiyan","Bahamas","Bahrain","Bangladesh","Barbados","Belgica","Belice","Benin","Bermudas","Bielorrusia","BirmaniaMyanmar","Bolivia","BosniayHerzegovina","Botswana","Brasil","Brunei","Bulgaria","BurkinaFaso","Burundi","Butan","CaboVerde","Camboya","Camerun","Canada","Chad","Chile","China","Chipre","Colombia","Comores","Congo","CoreadelNorte","CoreadelSur","CostadeMarfil","CostaRica","Croacia","Cuba","Dinamarca","Dominica","Ecuador","Egipto","ElSalvador","ElVaticano","EmiratosarabesUnidos","Eritrea","Eslovaquia","Eslovenia","España","EstadosUnidos","Estonia","Etiopia","Filipinas","Finlandia","Fiji","Francia","Gabon","Gambia","Georgia","Ghana","Gibraltar","Granada","Grecia","Guam","Guatemala","Guinea","GuineaEcuatorial","GuineaBissau","Guyana","Haiti","Honduras","Hungria","India","IndianOcean","Indonesia","Iran","Iraq","Irlanda","Islandia","Israel","Italia","Jamaica","Japon","Jersey","Jordania","Kazajstan","Kenia","Kirguistan","Kiribati","Kuwait","Laos","Lesoto","Letonia","Libano","Liberia","Libia","Liechtenstein","Lituania","Luxemburgo","Macedonia","Madagascar","Malasia","Malawi","Maldivas","Mali","Malta","Marruecos","Mauricio","Mauritania","Mexico","Micronesia","Moldavia","Monaco","Mongolia","Montserrat","Mozambique","Namibia","Nauru","Nepal","Nicaragua","Niger","Nigeria","Noruega","Nueva Zelanda","Oman","Paises Bajos","Pakistan","Palau","Panama","Papua Nueva Guinea","Paraguay","Peru","Polonia","Portugal","Puerto Rico","Qatar","Reino Unido","Republica Centroafricana","Republica Checa","Republica Democratica del Congo","Republica Dominicana","Ruanda","Rumania","Rusia","Sahara Occidental","Samoa","San Cristobal y Nevis","San Marino","San Vicente y las Granadinas","Santa Lucia","Santo Tome y Principe","Senegal","Seychelles","Sierra Leona","Singapur","Siria","Somalia","Southern Ocean","SriLanka","Swazilandia","Sudafrica","Sudan","Suecia","Suiza","Surinam","Tailandia","Taiwan","Tanzania","Tayikistan","Togo","Tokelau","Tonga","Trinidad y Tobago","Tunez","Turkmekistan","Turquia","Tuvalu","Ucrania","Uganda","Uruguay","Uzbekistan","Vanuatu","Venezuela","Vietnam","Yemen","Djibouti","Zambia","Zimbabue" ]
    
    var paises2 = [" "]
    
    var random = Int(arc4random_uniform(199))
    
    //TableView
    //Para conectar se arrastra hacia el ViewController (Bola Amarilla) y se selecciona delegate y data source
    
    @IBOutlet weak var tbl: UITableView!
    
    @IBAction func addCountry(_ sender: UIButton) {
        paises2.append(paises[random])
        paises.remove(at: random)
        tbl.reloadData()
    }
    //Filas por seccion
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return paises2.count
    }
    
    //Ejemplo: Celdas de Excel
    func tableView(_ tableView: UITableView,cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        
        //let value2 = Int(self.slider.value * 1)
        
        //cell.textLabel?.text = "\(value2) * \(indexPath.row + 1) = \(value2 * (indexPath.row + 1))"
        //cell.textLabel?.textAlignment = NSTextAlignment.center
        
        cell.textLabel?.text = paises2[indexPath.row]
        
        return cell
    }

    
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

